# From https://rise.readthedocs.io/en/stable/installation.html

using Conda
Conda.add_channel("conda-forge")
Conda.add("rise")

Conda.add("jupyter_nbextensions_configurator")
